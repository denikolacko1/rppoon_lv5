﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            Box first = new Box("Playing stuff");
            List<IShipable> shipables = new List<IShipable>();
            Product games = new Product("Two new games", 44.99, 1.25);
            ShippingService service = new ShippingService(2.5);
            shipables.Add(first);
            shipables.Add(games);
            double weight = 0;
            foreach(IShipable shipable in shipables) 
            {
                weight += shipable.Weight;
            }
            Console.WriteLine(service.Price(weight));
        }
    }
}
