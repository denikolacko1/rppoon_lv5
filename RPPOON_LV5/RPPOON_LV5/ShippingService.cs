﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV5
{
    class ShippingService
    {
        private double mass;
        public ShippingService (double mass)
        {
            this.mass = mass;
        }
        public double Price (double weight)
        {
            return mass * weight;
        }

    }
}
